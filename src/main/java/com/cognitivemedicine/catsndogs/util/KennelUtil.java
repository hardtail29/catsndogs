package com.cognitivemedicine.catsndogs.util;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.Collection;

import com.google.gson.reflect.TypeToken;

public class KennelUtil {

	public Collection<Cat> buildCatKennel() {
		Gson gson = new Gson();
		try(InputStream is = KennelUtil.class.getResourceAsStream("/Kennel_cats.json");) {

			Type collectionType  = new TypeToken<Collection<Cat>>(){}.getType();
			Collection<Cat> catz = gson.fromJson(new InputStreamReader(is), collectionType);
			return catz;
		}
	}

	public Collection<Dog> buildDogKennel() {
		Gson gson = new Gson();
		try(InputStream is = KennelUtil.class.getResourceAsStream("/Kennel_dogs.json");) {

			Type collectionType  = new TypeToken<Collection<Dog>>(){}.getType();
			Collection<Dog> dogz = gson.fromJson(new InputStreamReader(is), collectionType);
			return dogz;
		}
	}


}
