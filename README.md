Cognitive - Cat and Dogs Kennel project
=================================================================================================

	Committers:
			Steven Elliott (selliott@cognitivemedicine.com)

=================================================================================================
Major Usage
=================================================================================================

To test competence in some features of the Java 7 code base.

=================================================================================================

Dear Applicant:

Your assignment is to complete the following task in one(1) hour.  You may not have time to finish everything but finish as much as you can and stub the rest (HINT: this should encourage you to build the framework first with stubs then build the implementation).

Scenario:
You have recently purchased two kennels; one for dogs and one for cats.  At this time you want to combine both these kennels into one; Cats and Dogs.
Before you combine the kennels into one Mary would like to add a new cat to the new Cats kennel - name:Succatash license:4 owner:Mary (this can be done from the command line)
Combine the kennels into one.
Once the kennels 
Paul lost his dog Goofy and wants to know if Goofy is in the kennel.  If so he wants to change his name to Rex.
Finally print out the dogs and cats by owner name in alphabetical order.
To get you started a maven project has been created for you to download:

    https://anonymous@bitbucket.org/hardtail29/catsndogs.git

This repository contains the initial population of animals in the Cat kennel and the Dog kennel. Both populations can be found in src/main/resources:
    Kennel_cats.json
    Kennel_dogs.json

Both files are in JSON format.  In addition a utility class, KennelUtil, is provided to help you deserialize the Cats and Dogs from JSON once you have created the Cat and Dog classes.  As well the project pom has been configured to download the Gson library to aid in the creation of the initial kennels.  

If you prefer another JSON library (e.g. Jackson) please feel free to do so.

Objective:
The objective is to build reusable code that takes advantage of OO Java; polymorphism, encapsulation and inheritance.
The project should run from either the command line and/or from the ide.
You are free to use any resource available to you locally or from the web (Javadoc, Google..etc.).